package factory

import (
	"appliers/api-user/internal/repository"

	"gorm.io/gorm"
)

type UserFactory struct {
	UserRepository repository.UserRepository
}

func NewUserFactory(db *gorm.DB) *UserFactory {
	return &UserFactory{
		UserRepository: repository.NewUserRepositoryImplSql(db),
	}
}
