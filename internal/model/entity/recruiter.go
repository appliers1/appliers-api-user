package entity

type Recruiter struct {
	ID        uint   `json:"id"`
	CompanyID uint   `json:"company_id"`
	UserID    uint   `json:"user_id" gorm:"not null"`
	Name      string `json:"name" gorm:"size:100;not null"`
}
