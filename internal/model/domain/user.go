package domain

import (
	"appliers/api-user/pkg/constant"
	"os"
	"time"

	"github.com/golang-jwt/jwt"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type User struct {
	Username string              `json:"username" gorm:"size:100;unique;not null"`
	Email    string              `json:"email" gorm:"size:100;unique;not null"`
	Role     constant.UserRole   `json:"role" sql:"type:role" gorm:"type:enum('admin', 'applicant', 'company');not null"`
	Password string              `json:"password" gorm:"size:100;not null"`
	Status   constant.UserStatus `json:"status" sql:"type:status" gorm:"type:enum('active', 'pending', 'suspended', 'banned');default:'active';not null"`
	gorm.Model
}

// BeforeCreate is a method for struct User
// gorm call this method before they execute query
func (u *User) BeforeCreate(tx *gorm.DB) (err error) {
	u.CreatedAt = time.Now()
	u.HashPassword()
	return
}

// BeforeUpdate is a method for struct User
// gorm call this method before they execute query
func (u *User) BeforeUpdate(tx *gorm.DB) (err error) {
	u.UpdatedAt = time.Now()
	return
}

// HashPassword is a method for struct User for Hashing password
func (u *User) HashPassword() {
	bytes, _ := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	u.Password = string(bytes)
}

// GenerateToken is a method for struct User for creating new jwt token
func (u *User) GenerateToken(person int64, institution int64) (string, error) {
	var (
		jwtKey = os.Getenv("JWT_KEY")
	)
	/*
		ADMIN :
			Role = admin
			Person = -1
			Institution = -1

		Applicant :
			Role = applicant
			Person = {applicant_id}
			Institution = 0

		Company :
			Role = company
			Person = {recruiter_id}
			Institution = {company_id}
	*/

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"id":          u.ID,
		"username":    u.Username,
		"role":        u.Role,
		"person":      person,
		"institution": institution,
		"exp":         time.Now().Add(time.Hour * 72).Unix(), // we set expired in 72 hour
	})

	tokenString, err := token.SignedString([]byte(jwtKey))
	return tokenString, err
}
