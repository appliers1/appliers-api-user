package dto

import (
	"appliers/api-user/internal/model/entity"
	"appliers/api-user/pkg/constant"
)

// Login
type AuthLoginRequest struct {
	Identifier string `json:"identifier" validate:"required"`
	Password   string `json:"password" validate:"required"`
}
type AuthLoginResponse struct {
	ID       uint   `json:"id"`
	Token    string `json:"token"`
	Role     string `json:"role"`
	Username string `json:"username"`
	Email    string `json:"email"`
	Status   string `json:"status"`
}

// Register
type AuthRegisterRequest struct {
	Email    string            `validate:"required,email" json:"email"`
	Username string            `validate:"required" json:"username"`
	Role     constant.UserRole `validate:"required" json:"role"`
	Password string            `validate:"required" json:"password"`
	// Status   constant.UserStatus `validate:"required" json:"status"`
}
type AuthRegisterResponse struct {
	ID       uint   `json:"id"`
	Role     string `json:"role"`
	Username string `json:"username"`
	Email    string `json:"email"`
	Status   string `json:"status"`
}

type ApplicantRegisterRequest struct {
	AuthRegisterRequest
	Name         string          `json:"name"`
	PlaceOfBirth string          `json:"place_of_birth"`
	DateOfBirth  string          `json:"date_of_birth"`
	Gender       constant.Gender `json:"gender"`
}

type ApplicantRegisterResponse struct {
	AccountInfo  AuthRegisterResponse `json:"account_info"`
	Name         string               `json:"name"`
	PlaceOfBirth string               `json:"place_of_birth"`
	DateOfBirth  string               `json:"date_of_birth"`
	Gender       constant.Gender      `json:"gender"`
}

type CompanyRegisterRequest struct {
	AuthRegisterRequest
	Name    string                `json:"name"`
	Company entity.CompanyRequest `json:"company"`
}

type CompanyRegisterResponse struct {
	AccountInfo AuthRegisterResponse  `json:"account_info"`
	Name        string                `json:"name"`
	Company     entity.CompanyRequest `json:"company"`
}
