package dto

import "appliers/api-user/pkg/constant"

/* REQUEST */
type UserRequestBody struct {
	Email    string            `validate:"required,email" json:"email"`
	Username string            `validate:"required" json:"username"`
	Role     constant.UserRole `validate:"required" json:"role"`
	Password string            `validate:"required" json:"password"`
	// Status   constant.UserStatus `validate:"required" json:"status"`
}

type UpdateUserRequestBody struct {
	Email    *string              `validate:"omitempty,email" json:"email"`
	Username *string              `json:"username"`
	Role     *constant.UserRole   `json:"role"`
	Password *string              `json:"password"`
	Status   *constant.UserStatus `json:"status"`
}

// type UserRequestAuth struct {
// 	Identifier string `validate:"required" json:"identifier"`
// 	Password   string `validate:"required" json:"password"`
// }

/* RESPONSE */
type UserResponse struct {
	ID       uint                `json:"id"`
	Email    string              `json:"email"`
	Username string              `json:"username"`
	Role     constant.UserRole   `json:"role"`
	Status   constant.UserStatus `json:"status"`
	// CreatedAt time.Time `json:"created_at"`
	// UpdatedAt time.Time `json:"updated_at"`
}
