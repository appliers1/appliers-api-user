package repository

import (
	. "appliers/api-user/internal/model/domain"
	"appliers/api-user/internal/model/dto"
	"context"
	"strings"

	"gorm.io/gorm"
)

type UserRepository interface {
	Save(ctx context.Context, user *User) (*User, error)
	Update(ctx context.Context, user *User, userId uint) (*User, error)
	Delete(ctx context.Context, userId uint) error
	FindById(ctx context.Context, userId uint) (*User, error)
	FindByIdentifier(ctx context.Context, identifier string) (*User, error)
	FindAll(ctx context.Context, payload *dto.SearchGetRequest, p *dto.Pagination) ([]User, *dto.PaginationInfo, error)
	CountById(ctx context.Context, userId uint) (int, error)
	CountByIdentifier(ctx context.Context, identifier string) (int, error)
	CountByEmail(ctx context.Context, email string) (int, error)
	CountByUsername(ctx context.Context, email string) (int, error)
	MatchingEmailAndPassword(ctx context.Context, email string, password string) (bool, error)
}

type UserRepositoryImplSql struct {
	DB *gorm.DB
}

func NewUserRepositoryImplSql(db *gorm.DB) UserRepository {
	return &UserRepositoryImplSql{
		DB: db,
	}
}

func (repository *UserRepositoryImplSql) Save(ctx context.Context, user *User) (*User, error) {
	err := repository.DB.WithContext(ctx).Save(user).Error
	return user, err
}

func (repository *UserRepositoryImplSql) Update(ctx context.Context, user *User, userId uint) (*User, error) {
	err := repository.DB.WithContext(ctx).Model(user).Where("id = ?", userId).Updates(&user).Error
	updatedUser := User{}
	repository.DB.WithContext(ctx).First(&updatedUser, userId)
	return &updatedUser, err
}

func (repository *UserRepositoryImplSql) Delete(ctx context.Context, userId uint) error {
	err := repository.DB.WithContext(ctx).Delete(&User{}, userId).Error
	if err == nil {
		user := User{Status: "banned"}
		err = repository.DB.WithContext(ctx).Unscoped().Model(&user).Where("id = ?", userId).Updates(&user).Error
	}
	return err
}

func (repository *UserRepositoryImplSql) FindById(ctx context.Context, userId uint) (*User, error) {
	var user = User{}
	err := repository.DB.WithContext(ctx).First(&user, userId).Error
	if err == nil {
		return &user, nil
	} else {
		return nil, err
	}
}

func (repository *UserRepositoryImplSql) FindByIdentifier(ctx context.Context, identifier string) (*User, error) {
	var user = User{}
	err := repository.DB.WithContext(ctx).Where("username = ? or email = ? ", identifier, identifier).First(&user).Error
	if err == nil {
		return &user, nil
	} else {
		return nil, err
	}
}

func (repository *UserRepositoryImplSql) FindAll(ctx context.Context, payload *dto.SearchGetRequest, p *dto.Pagination) ([]User, *dto.PaginationInfo, error) {
	var users []User
	var count int64

	query := repository.DB.WithContext(ctx).Model(&User{})

	if payload.Search != "" {
		search := "%" + strings.ToLower(payload.Search) + "%"
		query = query.Where("lower(username) LIKE ? or lower(email) Like ? ", search, search)
	}

	countQuery := query
	if err := countQuery.Count(&count).Error; err != nil {
		return nil, nil, err
	}

	limit, offset := dto.GetLimitOffset(p)

	err := query.Limit(limit).Offset(offset).Find(&users).Error

	return users, dto.CheckInfoPagination(p, count), err
}

func (repository *UserRepositoryImplSql) CountById(ctx context.Context, userId uint) (int, error) {
	var count int64
	err := repository.DB.WithContext(ctx).Model(&User{}).Where("id = ?", userId).Count(&count).Error
	if err != nil {
		return -1, err
	}
	return int(count), nil
}

func (repository *UserRepositoryImplSql) CountByEmail(ctx context.Context, email string) (int, error) {
	var count int64
	err := repository.DB.WithContext(ctx).Unscoped().Model(&User{}).Where("email = ?", email).Count(&count).Error
	if err != nil {
		return -1, err
	}
	return int(count), nil
}

func (repository *UserRepositoryImplSql) CountByUsername(ctx context.Context, username string) (int, error) {
	var count int64
	err := repository.DB.WithContext(ctx).Unscoped().Model(&User{}).Where("username = ?", username).Count(&count).Error
	if err != nil {
		return -1, err
	}
	return int(count), nil
}

func (repository *UserRepositoryImplSql) CountByIdentifier(ctx context.Context, identifier string) (int, error) {
	var count int64
	err := repository.DB.WithContext(ctx).Model(&User{}).Where("username = ? OR email = ?", identifier, identifier).Count(&count).Error
	if err != nil {
		return -1, err
	}
	return int(count), nil
}

func (repository *UserRepositoryImplSql) MatchingEmailAndPassword(ctx context.Context, email string, password string) (bool, error) {
	var count int64
	err := repository.DB.WithContext(ctx).Model(&User{}).Where("email = ? AND password = ?", email, password).Count(&count).Error
	if err != nil {
		return false, err
	}

	if count == 1 {
		return true, nil
	} else {
		return false, nil
	}
}
