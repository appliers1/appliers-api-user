package controller

import (
	"appliers/api-user/internal/factory"
	"appliers/api-user/internal/model/dto"
	service "appliers/api-user/internal/service/auth"
	res "appliers/api-user/pkg/util/response"

	"github.com/labstack/echo/v4"
)

type AuthController struct {
	AuthService service.AuthService
}

func NewAuthController(f *factory.UserFactory) *AuthController {
	return &AuthController{
		AuthService: service.NewService(f),
	}
}

func (controller *AuthController) Auth(c echo.Context) error {
	payload := new(dto.AuthLoginRequest)
	if err := c.Bind(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	if err := c.Validate(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.Validation, err).Send(c)
	}

	// fmt.Println(payload)

	data, err := controller.AuthService.Auth(c.Request().Context(), payload)
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}

	return res.SuccessResponse(data).Send(c)
}

func (controller *AuthController) Register(c echo.Context) error {
	payload := new(dto.ApplicantRegisterRequest)
	if err := c.Bind(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	if err := c.Validate(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.Validation, err).Send(c)
	}

	data, err := controller.AuthService.RegisterApplicant(c.Request().Context(), payload)
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}

	return res.SuccessResponse(data).Send(c)
}

func (controller *AuthController) RegisterCompany(c echo.Context) error {
	payload := new(dto.CompanyRegisterRequest)
	if err := c.Bind(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	if err := c.Validate(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.Validation, err).Send(c)
	}

	data, err := controller.AuthService.RegisterCompany(c.Request().Context(), payload)
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}

	return res.SuccessResponse(data).Send(c)
}

func (controller *AuthController) RegisterAdmin(c echo.Context) error {
	payload := new(dto.AuthRegisterRequest)
	if err := c.Bind(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	if err := c.Validate(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.Validation, err).Send(c)
	}

	data, err := controller.AuthService.RegisterAdmin(c.Request().Context(), payload)
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}

	return res.SuccessResponse(data).Send(c)
}
