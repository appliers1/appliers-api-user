package controller

import (
	"appliers/api-user/internal/factory"
	"appliers/api-user/internal/model/dto"
	service "appliers/api-user/internal/service/user"
	"strconv"

	res "appliers/api-user/pkg/util/response"

	"github.com/labstack/echo/v4"
)

type UserController struct {
	UserService service.UserService
}

var err error

func NewUserController(f *factory.UserFactory) *UserController {
	return &UserController{
		UserService: service.NewUserServiceImpl(f),
	}
}

// func (controller *UserController) Create(c echo.Context) error {
// 	payload := new(dto.UserRequestBody)
// 	if err = c.Bind(payload); err != nil {
// 		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
// 	}

// 	user, err := controller.UserService.Create(c.Request().Context(), payload)
// 	if err != nil {
// 		return res.ErrorResponse(err).Send(c)
// 	}
// 	return res.SuccessResponse(user).Send(c)
// }

func (controller *UserController) Update(c echo.Context) error {
	payload := new(dto.UpdateUserRequestBody)
	err = c.Bind(payload)
	id, errId := strconv.ParseUint(c.Param("id"), 10, 32)
	if err != nil || errId != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	user, err := controller.UserService.Update(c.Request().Context(), payload, uint(id))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(user).Send(c)
}

func (controller *UserController) Delete(c echo.Context) error {
	var id, errId = strconv.ParseUint(c.Param("id"), 10, 32)
	if errId != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	err := controller.UserService.Delete(c.Request().Context(), uint(id))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(nil).Send(c)
}

func (controller *UserController) FindById(c echo.Context) error {
	var id, err = strconv.ParseUint(c.Param("id"), 10, 32)
	if err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	user, err := controller.UserService.FindById(c.Request().Context(), uint(id))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(user).Send(c)
}

func (controller *UserController) Find(c echo.Context) error {
	payload := new(dto.SearchGetRequest)
	if err := c.Bind(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	if err := c.Validate(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.Validation, err).Send(c)
	}

	result, err := controller.UserService.Find(c.Request().Context(), payload)
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}

	return res.CustomSuccessBuilder(200, result.Datas, "Get users success", nil).Send(c)
}
