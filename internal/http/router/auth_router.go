package router

import (
	"appliers/api-user/internal/http/controller"
	"appliers/api-user/internal/http/middleware"
	"appliers/api-user/pkg/constant"
	. "appliers/api-user/pkg/constant"

	"github.com/labstack/echo/v4"
)

func AuthRouter(e *echo.Echo, c *controller.AuthController) {
	e.POST("/auth", c.Auth, middleware.NewAllowedRole(EmptyRole).Authentication)
	e.POST("/register", c.Register, middleware.NewAllowedRole(EmptyRole).Authentication)
	e.POST("/register/company", c.RegisterCompany, middleware.NewAllowedRole(EmptyRole).Authentication)
	e.POST("/register/admin", c.RegisterAdmin, middleware.NewAllowedRole(map[UserRole]bool{constant.ROLE_ADMIN: true}).Authentication)
}
