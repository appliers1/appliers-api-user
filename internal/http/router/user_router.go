package router

import (
	"appliers/api-user/internal/http/controller"
	"appliers/api-user/internal/http/middleware"
	. "appliers/api-user/pkg/constant"

	"github.com/labstack/echo/v4"
)

func UserRouter(e *echo.Echo, c *controller.UserController) {
	e.GET("/users", c.Find, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true}).Authentication)
	e.GET("/users/:id", c.FindById, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true}).Authentication)
	e.PATCH("/users/:id", c.Update, middleware.NewAllowedRole(AllRole).Authentication)
	e.DELETE("/users/:id", c.Delete, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true}).Authentication)
}
