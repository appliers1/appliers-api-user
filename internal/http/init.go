package http

import (
	"appliers/api-user/internal/factory"
	c "appliers/api-user/internal/http/controller"
	"appliers/api-user/internal/http/router"

	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
)

func Init(e *echo.Echo, db *gorm.DB) {
	router.UserRouter(e, c.NewUserController(factory.NewUserFactory(db)))
	router.AuthRouter(e, c.NewAuthController(factory.NewUserFactory(db)))
}
