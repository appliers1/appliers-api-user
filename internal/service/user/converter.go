package user

import (
	. "appliers/api-user/internal/model/domain"
	"appliers/api-user/internal/model/dto"
)

func ToUserResponse(user User) dto.UserResponse {
	return dto.UserResponse{
		ID:       user.ID,
		Username: user.Username,
		Email:    user.Email,
		Role:     user.Role,
		Status:   user.Status,
		// CreatedAt: user.CreatedAt,
		// UpdatedAt: user.UpdatedAt,
	}
}

// func ToUserDomain(req *dto.UserRequestBody) *User {
// 	user := User{
// 		Email:    req.Email,
// 		Username: req.Username,
// 		Role:     req.Role,
// 		Password: req.Password,
// 		Status:   req.Status,
// 	}
// 	return &user
// }

func UpdateToUserDomain(req *dto.UpdateUserRequestBody) *User {
	user := User{}

	if req.Email != nil {
		user.Email = *req.Email
	}

	if req.Username != nil {
		user.Username = *req.Username
	}

	if req.Role != nil {
		user.Role = *req.Role
	}

	if req.Password != nil {
		user.Password = *req.Password
	}

	if req.Status != nil {
		user.Status = *req.Status
	}

	return &user
}
