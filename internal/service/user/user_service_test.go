package user

import (
	"appliers/api-user/internal/factory"
	_mockRepository "appliers/api-user/internal/mocks/repository"
	"appliers/api-user/internal/model/domain"
	"appliers/api-user/internal/model/dto"
	"appliers/api-user/pkg/constant"
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"golang.org/x/crypto/bcrypt"
)

var userRepository _mockRepository.UserRepository
var userService UserService
var userFactory = factory.UserFactory{
	UserRepository: &userRepository,
}
var userDomain domain.User

func setup() {
	userService = NewUserServiceImpl(&userFactory)
	passwd, _ := bcrypt.GenerateFromPassword([]byte("123"), bcrypt.DefaultCost)
	userDomain = domain.User{
		Username: "altaon",
		Email:    "alterra@dataon.com",
		Password: string(passwd),
		Role:     "admin",
		Status:   constant.STATUS_ACTIVE,
	}
}

func TestDelete(t *testing.T) {
	setup()
	userRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	userRepository.On("Delete",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()

	t.Run("Test Case 1 | Valid Delete", func(t *testing.T) {
		err := userService.Delete(context.Background(), uint(1))
		assert.Nil(t, err)
	})
}

func TestDeleteFail(t *testing.T) {
	setup()
	userRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, nil).Once()
	userRepository.On("Delete",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()

	t.Run("Test Case 2 | Invalid Delete", func(t *testing.T) {
		err := userService.Delete(context.Background(), uint(1))
		assert.NotNil(t, err)
	})

}

func TestUpdate(t *testing.T) {
	setup()
	userUpdate := domain.User{
		Username: "altaon1",
		Email:    "alterra@dataon.com",
		Password: "123",
		Role:     "admin",
		Status:   constant.STATUS_ACTIVE,
	}
	dto := dto.UpdateUserRequestBody{
		Username: &userUpdate.Username,
		Email:    &userUpdate.Email,
		Password: &userUpdate.Password,
		Role:     &userUpdate.Role,
		Status:   &userUpdate.Status,
	}
	userRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	userRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(&userDomain, nil).Once()
	userRepository.On("CountByUsername",
		mock.Anything,
		mock.AnythingOfType("string")).Return(0, nil).Once()
	userRepository.On("CountByEmail",
		mock.Anything,
		mock.AnythingOfType("string")).Return(0, nil).Once()
	userRepository.On("Update",
		mock.Anything,
		mock.Anything,
		mock.AnythingOfType("uint")).Return(&userUpdate, nil).Once()

	t.Run("Test Case 3 | Valid Update", func(t *testing.T) {

		user, _ := userService.Update(context.Background(), &dto, uint(1))
		assert.NotEqual(t, user.Username, userDomain.Username)
	})
}

func TestUpdateFail(t *testing.T) {
	setup()
	userUpdate := domain.User{
		Username: "altaon1",
		Email:    "alterra@dataon.com",
		Password: "123",
		Role:     "admin",
		Status:   constant.STATUS_ACTIVE,
	}
	dto := dto.UpdateUserRequestBody{
		Username: &userUpdate.Username,
		Email:    &userUpdate.Email,
		Password: &userUpdate.Password,
		Role:     &userUpdate.Role,
		Status:   &userUpdate.Status,
	}
	userRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, nil).Once()
	userRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()
	userRepository.On("CountByUsername",
		mock.Anything,
		mock.AnythingOfType("string")).Return(1, nil).Once()
	userRepository.On("CountByEmail",
		mock.Anything,
		mock.AnythingOfType("string")).Return(1, nil).Once()
	userRepository.On("Update",
		mock.Anything,
		mock.Anything,
		mock.AnythingOfType("uint")).Return(&userUpdate, nil).Once()
	t.Run("Test Case 4 | Invalid Update", func(t *testing.T) {

		_, err := userService.Update(context.Background(), &dto, uint(1))
		assert.NotNil(t, err)
	})
}

// func TestFindById(t *testing.T) {
// 	setup()
// 	userRepository.On("CountById",
// 		mock.Anything,
// 		mock.AnythingOfType("uint")).Return(1, nil).Once()
// 	userRepository.On("FindById",
// 		mock.Anything,
// 		mock.AnythingOfType("uint")).Return(&userDomain, nil).Once()

// 	t.Run("Test Case 5 | Valid FindById", func(t *testing.T) {
// 		user, _ := userService.FindById(context.Background(), uint(1))
// 		assert.Equal(t, user.Email, userDomain.Email)
// 	})
// }

func TestFindByIdFail(t *testing.T) {
	setup()
	userRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, nil).Once()
	userRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()

	t.Run("Test Case 6 | Invalid FindById", func(t *testing.T) {
		_, err := userService.FindById(context.Background(), uint(1))
		assert.NotNil(t, err)
	})
}

func TestFind(t *testing.T) {
	setup()
	var data []domain.User
	data = append(data, userDomain)
	page := 1
	limit := 10
	pagination := dto.Pagination{
		Page:     &page,
		PageSize: &limit,
	}
	userRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	userRepository.On("FindAll",
		mock.Anything,
		mock.Anything,
		mock.Anything).Return(data, dto.CheckInfoPagination(&pagination, 10), nil).Once()

	t.Run("Test Case 5 | Valid Find", func(t *testing.T) {

		dto := dto.SearchGetRequest{
			Pagination: pagination,
			Search:     "alte",
		}
		user, _ := userService.Find(context.Background(), &dto)
		assert.Equal(t, user.Datas[0].Email, userDomain.Email)
	})
}
