package user

import (
	"appliers/api-user/internal/factory"
	"appliers/api-user/internal/model/dto"
	"context"

	repository "appliers/api-user/internal/repository"
	res "appliers/api-user/pkg/util/response"
)

type UserService interface {
	Update(ctx context.Context, request *dto.UpdateUserRequestBody, userId uint) (*dto.UserResponse, error)
	Delete(ctx context.Context, userId uint) error
	FindById(ctx context.Context, userId uint) (*dto.UserResponse, error)
	Find(ctx context.Context, payload *dto.SearchGetRequest) (*dto.SearchGetResponse[dto.UserResponse], error)
}

type UserServiceImpl struct {
	UserRepository repository.UserRepository
}

func NewUserServiceImpl(f *factory.UserFactory) UserService {
	return &UserServiceImpl{
		UserRepository: f.UserRepository,
	}
}

func (service *UserServiceImpl) Update(ctx context.Context, request *dto.UpdateUserRequestBody, userId uint) (*dto.UserResponse, error) {

	count, err := service.UserRepository.CountById(ctx, userId)
	if err == nil {
		if count == 1 {

			if request.Username != nil || request.Email != nil {

				// get old data to compare
				user, err := service.UserRepository.FindById(ctx, userId)
				if err != nil {
					return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
				}

				if request.Username != nil {
					// check is username already used
					countUsername, err := service.UserRepository.CountByUsername(ctx, *request.Username)
					if err != nil {
						return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
					} else {
						if countUsername > 0 && *request.Username != user.Username {
							return nil, res.ErrorBuilder(&res.ErrorConstant.HasBeenUsed, err)
						}
					}
				}

				if request.Email != nil {
					// check is email already used
					countByEmail, err := service.UserRepository.CountByEmail(ctx, *request.Email)
					if err != nil {
						return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
					} else {
						if countByEmail > 0 && *request.Email != user.Email {
							return nil, res.ErrorBuilder(&res.ErrorConstant.HasBeenUsed, err)
						}
					}
				}

			}

			data, err := service.UserRepository.Update(ctx, UpdateToUserDomain(request), userId)

			if err == nil {
				data.ID = userId
				userResponse := ToUserResponse(*data)
				return &userResponse, nil
			}
		} else {
			if count == 0 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (service *UserServiceImpl) Delete(ctx context.Context, userId uint) error {
	count, err := service.UserRepository.CountById(ctx, userId)
	if err == nil {
		if count == 1 {
			err := service.UserRepository.Delete(ctx, userId)
			if err == nil {
				return nil
			}
		} else {
			if count == 0 {
				return res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (service *UserServiceImpl) FindById(ctx context.Context, userId uint) (*dto.UserResponse, error) {
	count, err := service.UserRepository.CountById(ctx, userId)
	if err == nil {
		if count == 1 {
			user, err := service.UserRepository.FindById(ctx, userId)
			if err == nil {
				userResponse := ToUserResponse(*user)
				return &userResponse, nil
			}
		} else {
			if count == 0 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (service *UserServiceImpl) Find(ctx context.Context, payload *dto.SearchGetRequest) (*dto.SearchGetResponse[dto.UserResponse], error) {

	users, info, err := service.UserRepository.FindAll(ctx, payload, &payload.Pagination)
	if err != nil {
		return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
	}

	var datas []dto.UserResponse

	for _, user := range users {
		datas = append(datas, ToUserResponse(user))
	}

	result := new(dto.SearchGetResponse[dto.UserResponse])
	result.Datas = datas
	result.PaginationInfo = *info

	return result, nil
}
