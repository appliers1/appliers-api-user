package auth

import (
	"appliers/api-user/internal/model/dto"
	"appliers/api-user/internal/model/entity"
	"appliers/api-user/internal/util/client"
	"appliers/api-user/pkg/constant"
	"bytes"
	"encoding/json"
	"net/http"
	"strconv"
	"time"
)

type CompanyHttpRequest interface {
	FetchRecruiter(userId uint) (*entity.Recruiter, error)
	SaveCompany(userId uint, payload *dto.CompanyRegisterRequest) (*entity.Company, error)
	SaveRecruiter(userId uint, companyId uint, payload *dto.CompanyRegisterRequest) (*entity.Recruiter, error)
}

type httpRequestToCompany struct {
	BaseUrl string
}

func NewHttpRequestToCompany() CompanyHttpRequest {
	return &httpRequestToCompany{
		BaseUrl: constant.CompanyServiceUrl,
	}
}

func (h *httpRequestToCompany) FetchRecruiter(userId uint) (*entity.Recruiter, error) {
	var userID string = strconv.Itoa(int(userId))
	httpRequest := client.ClientHttpRequest[entity.Recruiter]{
		Url:         h.BaseUrl + "/recruiters/users/" + userID,
		Method:      http.MethodGet,
		Payload:     nil,
		ContentType: constant.ContentTypeApplicationJson,
		Timeout:     10 * time.Second,
	}

	recruiter, err := httpRequest.StandardRequest()
	return recruiter, err
}

// values := map[string]string{"username": username, "password": password}
// jsonValue, _ := json.Marshal(values)
// resp, err := http.Post(authAuthenticatorUrl, "application/json", bytes.NewBuffer(jsonValue))

func (h *httpRequestToCompany) SaveCompany(userId uint, payload *dto.CompanyRegisterRequest) (*entity.Company, error) {
	values := map[string]interface{}{
		"name":            payload.Company.Name,
		"service":         payload.Company.Service,
		"total_employees": payload.Company.TotalEmployees,
	}
	jsonValue, err := json.Marshal(values)
	if err != nil {
		return nil, err
	}

	httpRequest := client.ClientHttpRequest[entity.Company]{
		Url:         h.BaseUrl + "/companies",
		Method:      http.MethodPost,
		Payload:     bytes.NewBuffer(jsonValue),
		ContentType: constant.ContentTypeApplicationJson,
		Timeout:     10 * time.Second,
	}

	company, err := httpRequest.StandardRequest()
	return company, err
}

func (h *httpRequestToCompany) SaveRecruiter(userId uint, companyId uint, payload *dto.CompanyRegisterRequest) (*entity.Recruiter, error) {
	values := map[string]interface{}{
		"company_id": companyId,
		"user_id":    userId,
		"name":       payload.Name,
	}
	jsonValue, err := json.Marshal(values)
	if err != nil {
		return nil, err
	}

	httpRequest := client.ClientHttpRequest[entity.Recruiter]{
		Url:         h.BaseUrl + "/recruiters",
		Method:      http.MethodPost,
		Payload:     bytes.NewBuffer(jsonValue),
		ContentType: constant.ContentTypeApplicationJson,
		Timeout:     10 * time.Second,
	}

	recruiter, err := httpRequest.StandardRequest()
	return recruiter, err
}
