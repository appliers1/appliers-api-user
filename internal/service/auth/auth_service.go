package auth

import (
	"appliers/api-user/internal/factory"
	"appliers/api-user/internal/model/domain"
	"appliers/api-user/internal/model/dto"
	"appliers/api-user/internal/model/entity"
	"appliers/api-user/internal/repository"
	"context"
	"fmt"

	"appliers/api-user/pkg/constant"
	res "appliers/api-user/pkg/util/response"

	"golang.org/x/crypto/bcrypt"
)

type AuthService interface {
	Auth(ctx context.Context, request *dto.AuthLoginRequest) (*dto.AuthLoginResponse, error)
	RegisterApplicant(ctx context.Context, payload *dto.ApplicantRegisterRequest) (*dto.ApplicantRegisterResponse, error)
	RegisterCompany(ctx context.Context, payload *dto.CompanyRegisterRequest) (*dto.CompanyRegisterResponse, error)
	RegisterAdmin(ctx context.Context, payload *dto.AuthRegisterRequest) (*dto.AuthRegisterResponse, error)
	IsIdentifierUnique(ctx context.Context, username *string, email *string) error
}

type service struct {
	UserRepository       repository.UserRepository
	CompanyHttpRequest   CompanyHttpRequest
	ApplicantHtppRequest ApplicantHttpRequest
}

func NewService(f *factory.UserFactory) AuthService {
	return &service{
		UserRepository:       f.UserRepository,
		CompanyHttpRequest:   NewHttpRequestToCompany(),
		ApplicantHtppRequest: NewHttpRequestToApplicant(),
	}
}

func (s *service) Auth(ctx context.Context, payload *dto.AuthLoginRequest) (*dto.AuthLoginResponse, error) {
	var result *dto.AuthLoginResponse
	data, err := s.UserRepository.FindByIdentifier(ctx, payload.Identifier)
	if data == nil {
		return result, res.ErrorBuilder(&res.ErrorConstant.EmailOrPasswordIncorrect, err)
	}

	if err = bcrypt.CompareHashAndPassword([]byte(data.Password), []byte(payload.Password)); err != nil {
		fmt.Printf("Hash : %s , password : %s\n", data.Password, payload.Password)
		return result, res.ErrorBuilder(&res.ErrorConstant.EmailOrPasswordIncorrect, err)
	}

	var personId, institutionId int64
	if data.Role == constant.ROLE_ADMIN {
		personId = -1
		institutionId = -1
	} else if data.Role == constant.ROLE_COMPANY {
		recruiter, err := s.CompanyHttpRequest.FetchRecruiter(data.ID)
		if err != nil {
			return result, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
		} else {
			personId = int64(recruiter.ID)
			institutionId = int64(recruiter.CompanyID)
		}
	} else if data.Role == constant.ROLE_APPLICANT {
		applicant, err := s.ApplicantHtppRequest.FetchApplicant(data.ID)
		if err != nil {
			return result, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
		} else {
			personId = int64(applicant.ID)
		}
		institutionId = 0
	}

	token, err := data.GenerateToken(personId, institutionId)
	if err != nil {
		return result, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
	}

	result = &dto.AuthLoginResponse{
		Token:    token,
		ID:       data.ID,
		Role:     string(data.Role),
		Username: data.Username,
		Email:    data.Email,
		Status:   string(data.Status),
	}

	return result, nil
}

func (s *service) RegisterApplicant(ctx context.Context, payload *dto.ApplicantRegisterRequest) (*dto.ApplicantRegisterResponse, error) {

	err := s.IsIdentifierUnique(ctx, &payload.Username, &payload.Email)
	if err != nil {
		return nil, err
	}

	if payload.Role != constant.ROLE_APPLICANT {
		return nil, res.ErrorBuilder(&res.ErrorConstant.BadRequest, err)
	}

	data, err := s.UserRepository.Save(ctx, toUserDomain(payload, constant.STATUS_ACTIVE))

	if err != nil {
		return nil, err
	} else {
		/*
			Jika applicant , maka simpan data ke data Aplicant berupa UserId dan Nama, default nya active
		*/
		applicant, err := s.ApplicantHtppRequest.SaveApplicant(data.ID, payload)
		if err != nil {
			s.UserRepository.Delete(ctx, data.ID)
			return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
		}
		fmt.Println(applicant)

		result := dto.ApplicantRegisterResponse{
			AccountInfo: dto.AuthRegisterResponse{
				ID:       data.ID,
				Username: data.Username,
				Email:    data.Email,
				Role:     string(data.Role),
				Status:   string(data.Status),
			},
			Name:         applicant.FullName,
			PlaceOfBirth: applicant.PlaceOfBirth,
			DateOfBirth:  applicant.DateOfBirth,
			Gender:       applicant.Gender,
		}
		return &result, nil
	}
}

func (s *service) RegisterCompany(ctx context.Context, payload *dto.CompanyRegisterRequest) (*dto.CompanyRegisterResponse, error) {
	err := s.IsIdentifierUnique(ctx, &payload.Username, &payload.Email)
	if err != nil {
		return nil, err
	}

	if payload.Role != constant.ROLE_COMPANY {
		return nil, res.ErrorBuilder(&res.ErrorConstant.BadRequest, err)
	}

	/*
		    Simpan data company ke company service
		   	Lalu simpan ke data recruiter berupa UserId dan Nama, dan companyId
			default nya adalah pending.
	*/

	data, err := s.UserRepository.Save(ctx, toUserDomain2(payload, constant.STATUS_PENDING))

	if err != nil {
		return nil, err
	} else {

		company, err := s.CompanyHttpRequest.SaveCompany(data.ID, payload)
		if err != nil {
			s.UserRepository.Delete(ctx, data.ID)
			return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
		}

		recruiter, err := s.CompanyHttpRequest.SaveRecruiter(data.ID, company.ID, payload)
		if err != nil {
			s.UserRepository.Delete(ctx, data.ID)
			return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
		}

		result := dto.CompanyRegisterResponse{
			AccountInfo: dto.AuthRegisterResponse{
				ID:       data.ID,
				Username: data.Username,
				Email:    data.Email,
				Role:     string(data.Role),
				Status:   string(data.Status),
			},
			Name: recruiter.Name,
			Company: entity.CompanyRequest{
				Name:           company.Name,
				Service:        company.Service,
				TotalEmployees: company.TotalEmployees,
			},
		}
		return &result, nil
	}
}

func (s *service) RegisterAdmin(ctx context.Context, payload *dto.AuthRegisterRequest) (*dto.AuthRegisterResponse, error) {
	err := s.IsIdentifierUnique(ctx, &payload.Username, &payload.Email)
	if err != nil {
		return nil, err
	}
	if payload.Role != constant.ROLE_ADMIN {
		return nil, res.ErrorBuilder(&res.ErrorConstant.BadRequest, err)
	}
	data, err := s.UserRepository.Save(ctx, toUserDomain3(payload, constant.STATUS_ACTIVE))
	if err != nil {
		return nil, err
	} else {
		result := dto.AuthRegisterResponse{
			ID:       data.ID,
			Username: data.Username,
			Email:    data.Email,
			Role:     string(data.Role),
			Status:   string(data.Status),
		}
		return &result, nil
	}
}

func (s *service) IsIdentifierUnique(ctx context.Context, username *string, email *string) error {
	countUsername, err := s.UserRepository.CountByUsername(ctx, *username)
	if err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
	} else {
		if countUsername > 0 {
			return res.ErrorBuilder(&res.ErrorConstant.HasBeenUsed, err)
		}
	}

	countEmail, err := s.UserRepository.CountByEmail(ctx, *email)
	if err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
	} else {
		if countEmail > 0 {
			return res.ErrorBuilder(&res.ErrorConstant.HasBeenUsed, err)
		}
	}

	return nil // true
}

func toUserDomain(req *dto.ApplicantRegisterRequest, status constant.UserStatus) *domain.User {
	user := domain.User{
		Email:    req.Email,
		Username: req.Username,
		Role:     req.Role,
		Password: req.Password,
		Status:   status,
	}
	return &user
}

func toUserDomain2(req *dto.CompanyRegisterRequest, status constant.UserStatus) *domain.User {
	user := domain.User{
		Email:    req.Email,
		Username: req.Username,
		Role:     req.Role,
		Password: req.Password,
		Status:   status,
	}
	return &user
}

func toUserDomain3(req *dto.AuthRegisterRequest, status constant.UserStatus) *domain.User {
	user := domain.User{
		Email:    req.Email,
		Username: req.Username,
		Role:     req.Role,
		Password: req.Password,
		Status:   status,
	}
	return &user
}
