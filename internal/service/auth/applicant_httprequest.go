package auth

import (
	"appliers/api-user/internal/model/dto"
	"appliers/api-user/internal/model/entity"
	"appliers/api-user/internal/util/client"
	"appliers/api-user/pkg/constant"
	"bytes"
	"encoding/json"
	"net/http"
	"strconv"
	"time"
)

type ApplicantHttpRequest interface {
	FetchApplicant(userId uint) (*entity.Applicant, error)
	SaveApplicant(userId uint, payload *dto.ApplicantRegisterRequest) (*entity.Applicant, error)
}

type httpRequestToApplicant struct {
	BaseUrl string
}

func NewHttpRequestToApplicant() ApplicantHttpRequest {
	return &httpRequestToApplicant{
		BaseUrl: constant.ApplicantServiceUrl,
	}
}

func (h *httpRequestToApplicant) FetchApplicant(userId uint) (*entity.Applicant, error) {
	var userID string = strconv.Itoa(int(userId))
	httpRequest := client.ClientHttpRequest[entity.Applicant]{
		Url:         h.BaseUrl + "/applicants/users/" + userID,
		Method:      http.MethodGet,
		Payload:     nil,
		ContentType: constant.ContentTypeApplicationJson,
		Timeout:     10 * time.Second,
	}

	recruiter, err := httpRequest.StandardRequest()
	return recruiter, err
}

// values := map[string]string{"username": username, "password": password}
// jsonValue, _ := json.Marshal(values)
// resp, err := http.Post(authAuthenticatorUrl, "application/json", bytes.NewBuffer(jsonValue))

func (h *httpRequestToApplicant) SaveApplicant(userId uint, payload *dto.ApplicantRegisterRequest) (*entity.Applicant, error) {

	values := map[string]interface{}{
		"user_id":        userId,
		"fullname":       payload.Name,
		"place_of_birth": payload.PlaceOfBirth,
		"date_of_birth":  payload.DateOfBirth,
		"gender":         payload.Gender,
	}
	jsonValue, err := json.Marshal(values)
	if err != nil {
		return nil, err
	}

	httpRequest := client.ClientHttpRequest[entity.Applicant]{
		Url:         h.BaseUrl + "/applicants",
		Method:      http.MethodPost,
		Payload:     bytes.NewBuffer(jsonValue),
		ContentType: constant.ContentTypeApplicationJson,
		Timeout:     10 * time.Second,
	}

	company, err := httpRequest.StandardRequest()
	return company, err
}
