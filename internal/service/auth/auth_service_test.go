package auth

// import (
// 	"appliers/api-user/internal/factory"
// 	_mockRepository "appliers/api-user/internal/mocks/repository"
// 	"appliers/api-user/internal/model/domain"
// 	"appliers/api-user/internal/model/dto"
// 	"appliers/api-user/pkg/constant"
// 	"context"
// 	"testing"

// 	"github.com/stretchr/testify/assert"
// 	"github.com/stretchr/testify/mock"
// 	"golang.org/x/crypto/bcrypt"
// )

// var userRepository _mockRepository.UserRepository
// var authService AuthService
// var userFactory = factory.UserFactory{
// 	UserRepository: &userRepository,
// }
// var userDomain domain.User

// func setup() {
// 	authService = NewService(&userFactory)
// 	passwd, _ := bcrypt.GenerateFromPassword([]byte("123"), bcrypt.DefaultCost)
// 	userDomain = domain.User{
// 		Username: "altaon",
// 		Email:    "alterra@dataon.com",
// 		Password: string(passwd),
// 		Role:     "admin",
// 		Status:   constant.STATUS_ACTIVE,
// 	}
// }

// func TestAuth(t *testing.T) {
// 	setup()
// 	userRepository.On("FindByIdentifier",
// 		mock.Anything,
// 		mock.AnythingOfType("string")).Return(&userDomain, nil).Once()

// 	t.Run("Test Case 1 | Valid Login", func(t *testing.T) {
// 		dto := dto.AuthLoginRequest{
// 			Identifier: "alterra@dataon.com",
// 			Password:   "123",
// 		}

// 		response, err := authService.Auth(context.Background(), &dto)
// 		assert.Nil(t, err)
// 		assert.Equal(t, response.Username, userDomain.Username)
// 	})

// }

// func TestAuthInvalidEmail(t *testing.T) {
// 	setup()
// 	userRepository.On("FindByIdentifier",
// 		mock.Anything,
// 		mock.AnythingOfType("string")).Return(nil, nil).Once()

// 	t.Run("Test Case 2 | Invalid Email", func(t *testing.T) {
// 		dto := dto.AuthLoginRequest{
// 			Identifier: "ss",
// 			Password:   "123",
// 		}

// 		_, err := authService.Auth(context.Background(), &dto)
// 		assert.NotNil(t, err)
// 	})
// }

// func TestAuthInvalidPass(t *testing.T) {
// 	setup()
// 	userRepository.On("FindByIdentifier",
// 		mock.Anything,
// 		mock.AnythingOfType("string")).Return(&userDomain, nil).Once()

// 	t.Run("Test Case 3 | Invalid Password", func(t *testing.T) {
// 		dto := dto.AuthLoginRequest{
// 			Identifier: "alterra@dataon.com",
// 			Password:   "ss",
// 		}
// 		_, err := authService.Auth(context.Background(), &dto)
// 		assert.NotNil(t, err)
// 	})
// }

// func TestRegister(t *testing.T) {
// 	setup()
// 	userRepository.On("CountByUsername",
// 		mock.Anything,
// 		mock.AnythingOfType("string")).Return(0, nil).Once()
// 	userRepository.On("CountByEmail",
// 		mock.Anything,
// 		mock.Anything).Return(0, nil).Once()
// 	userRepository.On("Save",
// 		mock.Anything,
// 		mock.Anything).Return(&userDomain, nil).Once()

// 	t.Run("Test Case 4 | Register", func(t *testing.T) {
// 		dto := dto.AuthRegisterRequest{
// 			Email:    "alterra@dataon.com",
// 			Username: "altaon",
// 			Role:     "admin",
// 			Status:   constant.STATUS_ACTIVE,
// 			Password: "ss",
// 		}
// 		user, _ := authService.Register(context.Background(), &dto)
// 		assert.Equal(t, user.Email, userDomain.Email)
// 		assert.Equal(t, user.Username, userDomain.Username)
// 	})
// }

// func TestRegisterFail(t *testing.T) {
// 	setup()
// 	dto := dto.AuthRegisterRequest{
// 		Email:    "alterra@dataon.com",
// 		Username: "altaon",
// 		Role:     "admin",
// 		Status:   constant.STATUS_ACTIVE,
// 		Password: "ss",
// 	}

// 	userRepository.On("CountByUsername",
// 		mock.Anything,
// 		mock.AnythingOfType("string")).Return(1, nil).Twice()
// 	userRepository.On("CountByEmail",
// 		mock.Anything,
// 		mock.Anything).Return(0, nil).Twice()

// 	t.Run("Test Case 5 | Register Fail Username Existed", func(t *testing.T) {

// 		_, err := authService.Register(context.Background(), &dto)
// 		assert.NotNil(t, err)
// 	})

// 	t.Run("Test Case 6 | Register Fail Email Existed", func(t *testing.T) {

// 		_, err := authService.Register(context.Background(), &dto)
// 		assert.NotNil(t, err)
// 	})
// }
