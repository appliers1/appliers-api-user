package seeder

import (
	"appliers/api-user/internal/model/domain"
	"appliers/api-user/pkg/constant"
	"log"

	"gorm.io/gorm"
)

func userTableSeeder(conn *gorm.DB) {

	var users = []domain.User{
		{
			Email:    "admin@appliers.com",
			Username: "admin",
			Role:     constant.ROLE_ADMIN,
			Password: "example",
			Status:   "active",
		},
		// {
		// 	Email:    "nadiarahma@dataon.com",
		// 	Username: "nadiarahma",
		// 	Role:     constant.ROLE_COMPANY,
		// 	Password: "example",
		// 	Status:   "active",
		// },
		// {
		// 	Email:    "rafiahmad@dataon.com",
		// 	Username: "rafi",
		// 	Role:     constant.ROLE_COMPANY,
		// 	Password: "example",
		// 	Status:   "active",
		// },
		// {
		// 	Email:    "dita@alterra.id",
		// 	Username: "dita",
		// 	Role:     constant.ROLE_COMPANY,
		// 	Password: "example",
		// 	Status:   "active",
		// },
		// {
		// 	Email:    "taqy@alterra.id",
		// 	Username: "taqy",
		// 	Role:     constant.ROLE_COMPANY,
		// 	Password: "example",
		// 	Status:   "active",
		// },
		// {
		// 	Email:    "tsuryanto16@gmail.com",
		// 	Username: "taufiq",
		// 	Role:     constant.ROLE_APPLICANT,
		// 	Password: "example",
		// 	Status:   "active",
		// },
		// {
		// 	Email:    "abiyyuimantara@gmail.com",
		// 	Username: "abiyyu",
		// 	Role:     constant.ROLE_APPLICANT,
		// 	Password: "example",
		// 	Status:   "active",
		// },
	}

	if err := conn.Create(&users).Error; err != nil {
		log.Printf("cannot seed data users, with error %v\n", err)
	}
	log.Println("success seed data users")
}
